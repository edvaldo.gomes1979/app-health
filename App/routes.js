import React from "react";
import {Scene, Schema, Actions, Stack, Animations, Router} from "react-native-router-flux";
import AppEventEmitter from "@themeServices/AppEventEmitter";

import {
  Auth,
  Dashboard,
  Splash,
  PregnancyCalculator
} from "@pages";


export default class RootRouter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const scenes = Actions.create(
      <Stack key="root" hideNavBar>
        <Scene key="splash"  component={Splash} initial={true} />
        <Scene key="auth"  component={Auth}/>
        <Scene key="dashboard" component={Dashboard} title="Dashboard"/>

      {/* Gravidez */}
      <Scene key="pregnancyCalculator" component={PregnancyCalculator} title="PregnancyCalculator"/>
      </Stack>
    );

    return (<Router hideNavBar={true}  scenes={scenes} />);
  }
}
