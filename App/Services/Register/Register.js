import { REST } from '@services/api';

class SingUpService{

  static sing_up(data, addEventListener){
    return REST.post('v1/users', data)
               .then(addEventListener);
  }
}

export default SingUpService;
