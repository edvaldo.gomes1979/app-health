import { REST } from '@services/api';

class PregnancyCalculatorService{
  /**
   * Cadastra um novo calculo de gravidez
   * 
   * @param  {Object}   data             Dados para cadastrar o calculo
   * @param  {Function} addEventListener Callback
   * @return {Promise}
   * 
   */
  static create(data, addEventListener){
    return REST.post('v1/person_health_records/pregnancy/calculators', data)
               .then(addEventListener);
  }
}

export default PregnancyCalculatorService;