import React from 'react';
import {ActivityIndicator, AsyncStorage} from 'react-native';

export class User{
  static current = {};

  static async set(data){
    this.current = data;
    await AsyncStorage.setItem('current_user', JSON.stringify(this.current));
    await AsyncStorage.setItem('current_user_access_token', this.current.access_token);
  }

  static load(addEventListener){
    AsyncStorage.getItem('current_user').then((data) => {
      this.current = data ? JSON.parse(data) : {};
      addEventListener(this.current);
    });
  }

  static accessToken(){
    return this.current.access_token;
  }

  static email(){
    return this.current.email;
  }
}
