import { REST } from '@services/api';

class AuthService{

  static login(data, addEventListener){
    return REST.post('v1/users/sign_in', data)
               .then(addEventListener);
  }
}

export default AuthService;