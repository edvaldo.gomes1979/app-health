import React  from 'react';
import API from '@constants/API';
import { User } from '@services/User';

export class REST {
  static _request(type, path, data){
    return new Promise((resolve, reject, ) => {
      User.load((current_user) => {
        let url     = this._url(path),
            headers = this._headers(),
            body    = this._data(data, method),
            method  = this._method(type);

        fetch(url, {...method, ...headers, ...body})
          .then((response) => response.json())
          .then(resolve)
          .catch(reject);
      });
    });
  }

  /**
   * Headers padrão para as requisições
   * 
   * @return {Object} Headers
   */
  static _headers(){
    let settings = {
      Accept:          'application/json',
      'Content-Type':  'application/json',
      'Authorization': API.services.authorization,
      'X-User-Email':   User.email(),
      'X-User-Token':   User.accessToken(),
    };
 
    return {headers: settings};
  }

  /**
   * Formata a URL que deve ser usado
   * 
   * @param  {String} path caminho do serviço
   * @return {String}      URL completa com o caminho do serviço
   */
  static _url(path = ''){
    let url = `${API.services.domain}${API.services.port ? ':' + API.services.port : ''}${API.services.namespace}`;
    return url + path;
  }

  /**
   * Formata os dados para enviar na requisição
   * 
   * @param  {Object} data   Dados para ser enviados na requisição
   * @param  {String} method Tipo de requisição
   * @return {Object}        Dados formatados
   */
  static _data(data, method){
    return {body: JSON.stringify(data)};
  }

  /**
   * Formata o método de requisição
   * 
   * @param  {String} method Método da requisição
   * @return {Object}        Método da requisição
   */
  static _method(method){
    return {method: method};
  }

  /**
   * Faz requisição do tipo GET
   * 
   * @param {String} path  Endereço do serviço
   * @param {Object} default {} Parametros da requisição
   */
  static get(path, query = {}){
    return this._request('GET', path, query);
  }

  /**
   * Faz requisição do tipo POST
   * 
   * @param {String} path  Endereço do serviço
   * @param {Object} default {} Parametros da requisição
   */
  static post(path, data = {}){
    return this._request('POST', path, data);
  }

  /**
   * Faz requisição do tipo PUT
   * 
   * @param {String} path  Endereço do serviço
   * @param {Object} default {} Parametros da requisição
   */
  static put(path, data = {}){
    return this._request('POST', path, data);
  }
  
  /**
   * Faz requisição do tipo DELETE
   * 
   * @param {String} path  Endereço do serviço
   * @param {Object} default {} Parametros da requisição
   */
  static delete(path, query = {}){
    return this._request('DELETE', path, query);
  }
};