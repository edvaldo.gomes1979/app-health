const Messages = {
  generic: {
    error: 'Ocorreu um erro inesperado. Tente novamente mais tarde.'
  }
};

export default Messages;