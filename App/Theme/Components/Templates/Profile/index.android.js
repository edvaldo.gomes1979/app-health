'use strict';
import React, {Component} from "react";
import {Notification, MyOrder, WishList} from "@templates";
import ScrollableTabView from "react-native-scrollable-tab-view";
import FacebookTabBar from "@custom/FacebookTabBar";

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {selectedTab: 'tabOne'}
  }

  render() {
    return (
      <ScrollableTabView
        tabBarPosition={"bottom"}
        locked={true}
        renderTabBar={() => <FacebookTabBar />}
      >
        <MyOrder tabLabel="md-cart"/>
        <WishList tabLabel="md-heart"/>
        <Notification tabLabel="md-notifications"/>
      </ScrollableTabView>
    );
  }
}

