'use strict';
import React, {Component} from "react";
import {TextInput, View, ScrollView} from "react-native";
import styles from "./styles";

export default class Shipping extends Component {
  render() {
    return (
      <View style={styles.layout}>
        <ScrollView>
          <View style={styles.card}>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'First name'}/>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'Last name'}/>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'Phone'}/>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'E-mail'}/>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'Country'}/>
            <View style={styles.inputContainer}>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.inputHalf} placeholder={'State'}/>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.inputHalf} placeholder={'City'}/>
            </View>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'Address'}/>
          </View>

        </ScrollView>
      </View>
    );
  }
}
