import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import {Platform} from "react-native";

const {width} = Dimensions.get("window"), vw = width / 100;

export default StyleSheet.create({
  "row": {
    "borderColor": "#eee",
    "marginTop": 10
  },
  "wrapper": {
    "backgroundColor": "rgba(255, 255, 255, 0.98)",
    "marginLeft": 12,
    "marginRight": 12,
    "borderRadius": 2,
    "overflow": "hidden",
    "flexDirection": "row"
  },
  "imageView": {
    "position": "relative",
    "margin": 8,
    "shadowColor": "#000",
    "shadowOpacity": 0.2,
    "shadowRadius": 2,
    "shadowOffset": {width: 0, height: 2},
    "backgroundColor": "#ffffff",
    "borderRadius": 3,
    "width": vw * 25,
    "height": vw * 25
  },
  "image": {
    "borderRadius": 3,
    "overflow": "hidden",
    "width": vw * 25,
    "height": vw * 25,
    "resizeMode": "cover"
  },
  "rating": {
    "fontSize": 11,
    "marginLeft": 8,
    "marginTop": 0,
    "marginBottom": 8,
    "color": "#999",
    "textAlign": "center"
  },
  "description": {
    "width": vw * 65
  },
  "title": {
    "fontSize": 17,
    "marginLeft": 12,
    "marginTop": 10,
    "marginRight": 8,
    "color": "#333333",
    "fontWeight": "400",
    "width": vw * 40
  },
  "content": {
    "fontSize": 13,
    "marginLeft": 12,
    "marginTop": 12,
    "marginRight": 8,
    "color": "#999",
    "fontWeight": "200",
    "width": vw * 60
  },
  "text": {
    "color": "#999999",
    "fontSize": 11,
    "marginRight": 20,
    "marginLeft": 4,
    "top": 2
  }
});