import React, {Component, PropTypes} from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import styles from './styles';

class Card extends Component {
  render = () => {
    const {title, content, image} = this.props.data;

    return (
      <View style={styles.wrapper}>
        <View>
          <View style={styles.imageView}>
            <Image style={styles.image} source={{uri: image}}></Image>
          </View>
        </View>

        <View style={styles.description}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.content}>{content}</Text>
        </View>
      </View>
    );
  }
}
export default Card;
