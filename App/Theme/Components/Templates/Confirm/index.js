'use strict';
import React, {Component} from "react";
import {Text, View, ScrollView} from "react-native";
import styles from "./styles";

export default class Confirm extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.body}>
          <View style={styles.card}>
            <View style={styles.row}>
              <Text style={[styles.label, styles.header]}>Cart</Text>
              <Text style={styles.link}>Edit</Text>
            </View>

            <View style={styles.row}>
              <View style={styles.labelView}>
                <Text style={styles.subLabel}>Lorem Ipsum Dolor Sit Amet Consect</Text>
                <Text style={styles.description}>Color: Blue, Size: XL</Text>
              </View>
              <Text style={styles.value}>%30</Text>
            </View>

            <View style={styles.row}>
              <Text style={styles.label}>Sit Amet Consectetur Elit Sed</Text>
              <Text style={styles.value}>$19</Text>
            </View>

            <View style={styles.row}>
              <View style={styles.labelView}>
                <Text style={styles.subLabel}>Dolor Sit Amet Consect</Text>
                <Text style={styles.description}>Size: XLL</Text>
              </View>
              <Text style={styles.value}>$50</Text>
            </View>

            <View style={[styles.row, styles.linebreak]}>
              <Text style={styles.label}>TOTAL</Text>
              <Text style={[styles.value, styles.total]}>$99</Text>
            </View>
          </View>
          <View style={styles.card}>
            <View style={styles.row}>
              <Text style={[styles.label, styles.header]}>Delivery</Text>
              <Text style={styles.link}>Edit</Text>
            </View>

            <View style={styles.row}>
              <Text style={[styles.label, styles.fullWidth]}>Address: Lorem Ipsum Dolor Sit Amet Consectetur
                Adipisicing Elit Sed Do Eiusmod Tempor
              </Text>
            </View>
          </View>
          <View style={styles.card}>
            <View style={styles.row}>
              <Text style={[styles.label, styles.header]}>Payment</Text>
              <Text style={styles.link}>Edit</Text>
            </View>

            <View style={styles.row}>
              <Text style={styles.label}>VISA XXXX-XXXX-XXXX-1234
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
};
