'use strict';
import React, {Component} from "react";
import {Text, View, ScrollView} from "react-native";
import styles from "./styles";
import * as Animatable from "react-native-animatable";

export default class Tracking extends Component {
  render() {
    return (
      <View style={styles.body}>
        <View style={styles.card}>
          <ScrollView>
            <View style={styles.line}/>
            <View style={styles.lineFinish}/>

            <View style={styles.row}>
              <View style={[styles.circel, styles.finish]} />
              <Text style={[styles.title, styles.titleFinish]}>PLACED</Text>
            </View>

            <View style={styles.content}>
              <View>
                <Text style={styles.label}>Lorem Ipsum Dolor Sit Amet Consectetur Adipi</Text>
                <Text style={styles.date}>2 Jul 2016</Text>
              </View>
              <View>
                <Text style={styles.label}>Adipisicing Elit Sed Do Eiusmod Tempo Do Eiusmod Tempor Lorem
                  Ipsum Dolor Sit</Text>
                <Text style={styles.date}>2 Jul 2016</Text>
              </View>
              <View>
                <Text style={styles.label}>Adipisicing Elit Sed Do Eiusmod Tempor Incididunt</Text>
                <Text style={styles.date}>6 Jul 2016</Text>
              </View>
            </View>

            <View style={styles.row}>
              <Animatable.View animation="pulse" easing="ease-out" iterationCount="infinite">
                <View style={[styles.circel, styles.going]}></View>
              </Animatable.View>
              <Text style={styles.title}>DISPATCHING</Text>
            </View>
            <View style={styles.content}>
              <View>
                <Text style={styles.label}>Adipisicing Elit Sed Do Eiusmod Tempo Do Eiusmod Tempor
                  Lorem Ipsum Dolor Sit</Text>
                <Text style={styles.date}>2 Jul 2016</Text>
              </View>
              <View>
                <Text style={styles.label}>Adipisicing Elit Sed Do Eiusmod Tempor Incididunt</Text>
                <Text style={styles.date}>6 Jul 2016</Text>
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.circel} />
              <Text style={styles.title}>DELIVERED</Text>
            </View>
            <View style={styles.content}>
              <View>
                <Text style={styles.label}>Adipisicing Elit Sed Do Eiusmod Tempor Incididunt</Text>
                <Text style={styles.date}>6 Jul 2016</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}