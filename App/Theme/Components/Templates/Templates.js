'use strict';

import React, {Component} from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import css from "@styles/style";
import {Actions} from "react-native-router-flux";
import Toolbar from "@controls/Toolbar";

export default class Template extends Component {
  render() {
    return (
      <View style={css.layout}>
        <Toolbar name="" />
        <ScrollView>
          <View style={css.templateLayout}>
            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.news}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-wordpress.png')}></Image>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.wooProduct}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-woocommerce.png')}></Image>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.intro}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/intro.png')}></Image>
              <Text style={css.templateMenu}>Intro</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.introAnimate}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/intro2.png')}></Image>
              <Text style={css.templateMenu}>Intro</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.map}>

              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/map.png')}></Image>
              <Text style={css.templateMenu}>Map</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.login}>

              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/signup.png')}></Image>
              <Text style={css.templateMenu}>Sign In</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.home}>

              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/category.png')}></Image>
              <Text style={css.templateMenu}>Categories</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.product}>

              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/products.png')}></Image>
              <Text style={css.templateMenu}>Product Simple</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.productgrid}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/product.png')}></Image>
              <Text style={css.templateMenu}>Product Grid</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.cart}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/cart.png')}></Image>
              <Text style={css.templateMenu}>Cart</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.checkout}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/Cash.png')}></Image>
              <Text style={css.templateMenu}>Checkout</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.complete}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/Thumbs.png')}></Image>
              <Text style={css.templateMenu}>Complete</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.myorder}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/myorder.png')}></Image>
              <Text style={css.templateMenu}>My Order</Text>
            </TouchableOpacity>


            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.trackorder}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/shipping.png')}></Image>
              <Text style={css.templateMenu}>Shipping</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.profile}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/profile.png')}></Image>
              <Text style={css.templateMenu}>Profile</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.wishlist}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/Favorite.png')}></Image>
              <Text style={css.templateMenu}>Wish list</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.notification}>
              <Image style={css.templateImage}
                     source={require('@themeImages/icon-set/user.png')}></Image>
              <Text style={css.templateMenu}>Notification</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
