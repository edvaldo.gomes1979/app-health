'use strict';
import React, {Component} from "react";
import {Text, View, Image, TouchableOpacity, ScrollView} from "react-native";
import {Actions} from "react-native-router-flux";
import styles from "./styles";
import {Toolbar} from "@controls";

export default class MyOrder extends Component {
  render() {
    return (
      <View style={styles.body}>
        <Toolbar name="My Order"/>

        <ScrollView>
          <View style={styles.card}>
            <View style={styles.row}>
              <Image style={styles.image} source={require('@themeImages/man-s2.png')}/>

              <View style={styles.content}>
                <Text style={styles.name}>In said to of poor full be post face snug</Text>
                <Text style={styles.status}>Order Id: 1230922</Text>
                <Text style={styles.status}>Status: Delivered Successfully!</Text>
                <Text style={styles.status}>Delivered on: 4 Jul 2016</Text>

                <View style={styles.buttons}>
                  <TouchableOpacity>
                    <Text style={styles.link}>DETAIL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={Actions.trackorder}>
                    <Text style={styles.small}>TRACK</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.line}/>
            <View style={styles.lineFinishHalf}/>

            <View style={styles.statusIcons}>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.finish]}/>
                <Text style={styles.iconLabel}>PLACED</Text>
              </View>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.going]}/>
                <Text style={styles.iconLabel}>DISPATCHED</Text>
              </View>
              <View style={styles.icon}>
                <View style={[styles.circel]}/>
                <Text style={styles.iconLabel}>DELIVERED</Text>
              </View>
            </View>
          </View>

          <View style={styles.card}>
            <View style={styles.row}>
              <Image style={styles.image} source={require('@themeImages/man-s1.png')}/>

              <View style={styles.content}>
                <Text style={styles.name}>Adipisicing Elit Sed Do Eiusmod Tempor Incididunt Ut Labore Et
                  Do
                </Text>
                <Text style={styles.status}>Order Id: 1230932</Text>
                <Text style={styles.status}>Status: Delivered Successfully!</Text>
                <Text style={styles.status}>Delivered on: 1 Jul 2016</Text>

                <View style={styles.buttons}>
                  <TouchableOpacity>
                    <Text style={styles.link}>DETAIL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={Actions.trackorder}>
                    <Text style={styles.small}>TRACK</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.line}/>
            <View style={styles.lineFinish}/>
            <View style={styles.statusIcons}>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.finish]}/>
                <Text style={styles.iconLabel}>PLACED</Text>
              </View>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.finish]}/>
                <Text style={styles.iconLabel}>DISPATCHED</Text>
              </View>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.going]}/>
                <Text style={styles.iconLabel}>DELIVERED</Text>
              </View>
            </View>
          </View>

          <View style={styles.card}>
            <View style={styles.row}>
              <Image style={styles.image} source={require('@themeImages/man-s4.png')}/>

              <View style={styles.content}>
                <Text style={styles.name}>Sit Amet Consectetur Adipisicing Elit Sed Do Eius
                </Text>
                <Text style={styles.status}>Order Id: 1232322</Text>
                <Text style={styles.status}>Status: Delivered Successfully!</Text>
                <Text style={styles.status}>Delivered on: 1 Jul 2016</Text>

                <View style={styles.buttons}>
                  <TouchableOpacity>
                    <Text style={styles.link}>DETAIL</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={Actions.trackorder}>
                    <Text style={styles.small}>TRACK</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.line}/>
            <View style={styles.lineFinish}/>

            <View style={styles.statusIcons}>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.finish]}/>
                <Text style={styles.iconLabel}>PLACED</Text>
              </View>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.finish]}/>
                <Text style={styles.iconLabel}>DISPATCHED</Text>
              </View>
              <View style={styles.icon}>
                <View style={[styles.circel, styles.finish]}/>
                <Text style={styles.iconLabel}>DELIVERED</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    )
      ;
  }
}
