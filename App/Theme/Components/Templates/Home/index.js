import React, {Component} from "react";
import {View, Platform, TextInput} from "react-native";
import ScrollableTabView, {ScrollableTabBar} from "react-native-scrollable-tab-view";
import Icon from "@expo/vector-icons/EvilIcons";
import ListItem from "@layout/ListItem";
import HorizontalItem from "@layout/HorizontalItem";
import AppEventEmitter from "@themeServices/AppEventEmitter";
import styles from "./styles";
import Toolbar from "@controls/Toolbar";
import Category from "@templates/Category";

export default class Home extends Component {
  open() {
    AppEventEmitter.emit('hamburger.click');
  }

  render() {
    return (
      <View style={styles.color}>
        <Toolbar name='Shop' searchButton={true}/>

        <View style={[styles.search]}>
          <TextInput
            underlineColorAndroid='rgba(0,0,0,0)'
            style={styles.searchBar}
            placeholder={'Looking for a gift..'}
            selectionColor={'#7d59c8'}/>
          <Icon name={'search'} style={styles.searchIcon}/>
        </View>

        <View style={styles.menu}>
          <ScrollableTabView
            initialPage={0}
            locked={true}
            tabBarUnderlineStyle={ {height: 2, backgroundColor: "#393800"}  }
            tabBarActiveTextColor={"#393838"}
            tabBarInactiveTextColor={"#B8B8B8"}
            tabBarTextStyle={{height: 20, fontWeight: 'normal', fontSize: 13}}

            renderTabBar={() => <ScrollableTabBar
              underlineHeight={2}
              tabsContainerStyle={{height: (Platform.OS === 'ios' ? 38 : 49)}}
              tabStyle={{paddingBottom: 0, borderBottomWidth: 0, paddingTop: 0, paddingLeft: 10, paddingRight: 10}}
            />}>

            <Category tabLabel="Categories"/>
            <HorizontalItem tabLabel="Man"/>
            <ListItem tabLabel="Women"/>
          </ScrollableTabView>
        </View>
      </View>
    );
  }
}
