'use strict';

import React, {Component} from "react";
import {View, Image, TouchableOpacity, ScrollView, TextInput} from "react-native";
import styles from "./styles";

export default class Payment extends Component {
  render() {
    return (
      <View style={styles.layout}>
        <ScrollView>
          <View style={styles.cardContainer}>
            <TouchableOpacity>
              <Image source={require('@themeImages/card-1.png')} style={styles.bankImage}/>
            </TouchableOpacity>
            <TouchableOpacity>
              <Image source={require('@themeImages/card-2.png')} style={styles.bankImage}/>
            </TouchableOpacity>
            <TouchableOpacity>
              <Image source={require('@themeImages/card-3.png')} style={styles.bankImage}/>
            </TouchableOpacity>
          </View>

          <View style={styles.card}>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'Cardholder Name'}/>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'Card Number'}/>

            <View style={{flex: 1, flexDirection: 'column'}}>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'MM'}/>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'YY'}/>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={styles.input} placeholder={'CVV'}/>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
};
