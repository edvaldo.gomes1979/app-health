'use strict';
import React, {Component} from "react";
import {Platform, Text, View, TouchableOpacity} from "react-native";
import * as Animatable from "react-native-animatable";
import {Actions} from "react-native-router-flux";
import Icon from "@expo/vector-icons/EvilIcons";
import {Toolbar, ButtonRound} from "@controls";
import styles from "./styles";

export default class Complete extends Component {
  render() {
    const getStatus = () => {
      if (Platform.OS === 'ios') {
        return <View style={styles.statusContainer}>
          <View style={styles.status}>
            <TouchableOpacity onpress={Actions.checkout}>
              <View style={[styles.statusIcon, styles.statusFinish]}/>
              <Text style={[styles.tabName, styles.tabNameActive]}>Shipping</Text>
            </TouchableOpacity>

            <TouchableOpacity>
              <View style={[styles.statusIcon, styles.statusFinish]}/>
              <Text style={[styles.tabName, styles.tabNameActive]}>Payment</Text>
            </TouchableOpacity>

            <TouchableOpacity>
              <View style={[styles.statusIcon, styles.statusFinish]}/>
              <Text style={[styles.tabName, styles.tabNameActive]}>Confirm</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.status, styles.statusFull, styles.statusComplete]}/>
        </View>
      }
    }
    return (
      <View style={styles.layout}>
        <Toolbar name="Successfully" isChild={true}/>

        {getStatus()}

        <View style={styles.successView}>
          <Animatable.Text animation="bounceInDown" easing="ease-out" style={{textAlign: 'center'}}>
            <Icon name={'like'} style={styles.checkIcon}/>
          </Animatable.Text>

          <Text style={styles.h1}>Congratulations. Your order is accepted.</Text>

          <Text style={styles.body}>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
            minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
        </View>

        <TouchableOpacity style={{marginBottom: 16}}>
          <ButtonRound text="Track order" onPress={Actions.trackorder}/>
        </TouchableOpacity>
      </View>
    )
  }
};
