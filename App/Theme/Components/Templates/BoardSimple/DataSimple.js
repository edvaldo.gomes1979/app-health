export default [
  {
    title: 'Lorem Ipsum',
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
    iconImage: require('@themeImages/intro/flower-pot.png'),
  },
  {
    title: 'Consectetur',
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
    iconImage: require('@themeImages/intro/footprints.png'),
  },
  {
    title: 'Adipisicing elitt',
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
    iconImage: require('@themeImages/intro/happy-sun.png'),
  },

  {
    title: 'sit amet',
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
    iconImage: require('@themeImages/intro/holly.png'),
  },
  {
    title: 'Sed do eiusmod',
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
    iconImage: require('@themeImages/intro/kite.png'),
  },
]
