'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, ScrollView} from "react-native";
import styles from "./styles";
import {Toolbar} from "@controls";
import Icon from "@expo/vector-icons/Ionicons";

export default class Notification extends Component {
  render() {
    const unreadCard = (
      <TouchableOpacity style={styles.card}>
        <View style={{flexDirection: 'row'}}>
          <Icon name={'ios-notifications'} style={styles.unreadBell}/>

          <View style={{marginLeft: 10}}>
            <Text style={styles.unreadHeader}>Simply dummy text of the printing.</Text>

            <View style={styles.blockText}>
              <Text style={styles.unreadText}>Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been industry's standard....</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );

    const readCard = (
      <TouchableOpacity style={styles.card}>
        <View style={{flexDirection: 'row'}}>
          <Icon name={'ios-notifications-outline'} style={styles.readBell}/>

          <View style={{marginLeft: 10}}>
            <Text style={styles.readHeader}>Lorem Ipsum has been industry's standard...</Text>

            <View style={styles.blockText}>
              <Text style={styles.readText}>Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been industry's standard....</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );

    return (
      <View style={{flex: 1, backgroundColor: '#eee'}}>
        <Toolbar name="Notification"/>
        <ScrollView>
          {unreadCard}
          {unreadCard}
          {unreadCard}
          {unreadCard}
          {readCard}
          {readCard}
          {readCard}
          {readCard}
          {readCard}
          {readCard}
          {readCard}
          {readCard}
          {readCard}
        </ScrollView>
      </View>
    );
  }
}
