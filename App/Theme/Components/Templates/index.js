import Intro from "./Intro";
import Login from "./Login";
import Home from "./Home";
import Product from "./Product";
import Profile from "./Profile";
import Notification from "./Notification";
import Complete from "./Complete";
import ProductDetail from "./ProductDetail";
import Checkout from "./Checkout";
import TrackOrder from "./TrackOrder";
import Cart from "./Cart";
import WishList from "./WishList";
import ProductGrid from "./Product/ProductGrid";
import MyOrder from "./MyOrder";
import BoardSimple from "./BoardSimple";
import Map from "./Map";
import Templates from "./Templates";
import Shipping from "./Shipping";
import Confirm from "./Confirm";
import Payment from "./Payment";
import Summary from "./Summary";
import Tracking from "./Tracking";


export {
  Templates,
  Confirm,
  Tracking,
  Summary,
  Payment,
  Shipping,
  Login,
  Intro,
  Home,
  Product,
  Profile,
  Notification,
  Complete,
  ProductDetail,
  Checkout,
  TrackOrder,
  Cart,
  WishList,
  ProductGrid,
  MyOrder,
  BoardSimple,
  Map
};
