'use strict';

import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import AppEventEmitter from '@themeServices/AppEventEmitter';
import product from "./styles";
import {Toolbar} from '@controls';
import css from "@styles/style";

export default class ProductGrid extends Component {
  open() {
    AppEventEmitter.emit('hamburger.click');
  }

  render() {
    return (
      <View style={product.color}>
        <Toolbar name="Product" gridButton={true} heartButton={true}/>

        <ScrollView style={{marginTop: 5}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman-1.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Frame Cardigan</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice, {paddingBottom: 12, paddingLeft: 4}]}>$69.95</Text>
              </View>
            </View>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman2.jpg')} style={product.productItemGrid}></Image>

              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>

              <Text style={product.productName}>Bodycon Dress</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$49.94</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman4.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Bodycon Dress</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$49.94</Text>
              </View>
            </View>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman5.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Frame Cardigan</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$69.95</Text>
              </View>
            </View>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman6.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Frame Cardigan</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$69.95</Text>
              </View>
            </View>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman7.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Frame Cardigan</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$69.95</Text>
              </View>
            </View>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman3.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Frame Cardigan</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$69.95</Text>
              </View>
            </View>
            <View style={product.cardsGrid}>
              <Image source={require('@themeImages/woman8.jpg')} style={product.productItemGrid}></Image>
              <TouchableOpacity style={product.star}>
                <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}></Image>
              </TouchableOpacity>
              <Text style={product.productName}>Frame Cardigan</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={[product.discountPrice]}>$69.95</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
