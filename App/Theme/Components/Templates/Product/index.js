'use strict';
import React, {Component} from "react";
import {
  TextInput,
  RefreshControl,
  Text,
  Animated,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import css from "@styles/style";
import styles from "./styles";
import Toolbar from "@controls/Toolbar";
import AppEventEmitter from "@themeServices/AppEventEmitter";

let offset = 0;
let alpha = 100;
let beta = 50;

export default class Product extends Component {
  open() {
    AppEventEmitter.emit('hamburger.click');
  }

  constructor(props) {
    super(props);
    this.state = {
      _animatedMenu: new Animated.Value(0),
      isRefreshing: false
    }
  }

  hideMenu() {
    Animated.spring(this.state._animatedMenu, {
      toValue: -120
    }).start();
  }

  showMenu() {
    Animated.spring(this.state._animatedMenu, {
      toValue: 0
    }).start();
  }

  componentDidMount() {
    this._onRefresh()
  }

  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;

    if (currentOffset < alpha) {
      return;
    }

    if (Math.abs(offset - currentOffset) <= beta)
      return;

    if (this.state.isRefreshing) {
      return;
    }

    if (currentOffset > offset) {
      this.hideMenu();
      console.log('down');
    } else if (currentOffset < offset) {
      this.showMenu();
      console.log('up');
    }

    offset = currentOffset;
  }

  _onRefresh = () => {
    this.hideMenu();

    this.setState({
      isRefreshing: true
    });

    setTimeout(() => {
      this.showMenu();

      this.setState({
        loaded: this.state.loaded + 10,
        isRefreshing: false,
      });
    }, 1000);
  }

  render() {
    return (
      <View style={styles.color}>
        <Animated.View style={[css.toolbarView, {transform: [{translateY: this.state._animatedMenu}]}]}>
          <Toolbar name="Product" heartButton={true} layoutButton={true}/>
          <TextInput underlineColorAndroid='rgba(0,0,0,0)' style={css.inputSearch} placeholder={'Search'}/>
        </Animated.View>

        <ScrollView
          onScroll={this.onScroll.bind(this)} scrollEventThrottle={30}
          refreshControl={<RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={this._onRefresh}
            tintColor="#333"
            title="Loading..."
            titleColor="#333"
            colors={['#333', '#999', '#ddd']}
            progressBackgroundColor="#ffff00"
          />
          }>
          <TouchableOpacity style={[styles.cards, {marginTop: 110}]}>
            <Image source={require('@themeImages/woman-1.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Frame Cardigan</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$69.95</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cards}>
            <Image source={require('@themeImages/woman2.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Bodycon Dress</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$49.94</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cards}>
            <Image source={require('@themeImages/woman4.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Bodycon Dress</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$49.94</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cards}>
            <Image source={require('@themeImages/woman6.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Bodycon Dress</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$49.94</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cards}>
            <Image source={require('@themeImages/woman7.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Bodycon Dress</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$49.94</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cards}>
            <Image source={require('@themeImages/woman3.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Bodycon Dress</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$49.94</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.cards}>
            <Image source={require('@themeImages/woman8.jpg')} style={styles.productItem}/>
            <TouchableOpacity style={styles.star}>
              <Image source={require('@themeImages/icon-heart.png')} style={css.imageIcon}/>
            </TouchableOpacity>
            <Text style={styles.productName}>Bodycon Dress</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.discountPrice, {paddingBottom: 12}]}>$49.94</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}
