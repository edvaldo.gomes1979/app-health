import React, {Component} from "react";
import {View, Text, Animated, ScrollView, TouchableOpacity} from "react-native";
import {Toolbar} from "@controls";
import css from "@common/style";
import news from "./style";
import {Actions} from "react-native-router-flux";
import categories from "@mockdata/categories";

export default class PostMenu extends Component {
	constructor(props) {
		super(props);

		this.state = {
			paddingTop: 100,
			categories: categories,
			categoryActive: null,
			_animatedMenu: this.props.animateMenu
		};
	}

	selectTab(categoryId) {
		this.setActiveCategory(categoryId);
	}

	upCaseTitle(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase().replace(' &amp;', '');
	}

	setActiveCategory(cateId) {
		// this.setState({categoryActive: cateId});
		// EventEmitter.emit('posts.updateCategory', {category: cateId});
		// EventEmitter.emit('posts.updateBanner', {category: cateId});
	}

	updateLayout() {
		this.setState({oneColumnLayout: !this.state.oneColumnLayout});
	}

	render() {
		if (this.state.categories === null) {
			return <Toolbar css={news.toolbar} action={Actions.news} listViewButton={true}  newsLayoutButton={true} cardButton={true} listButton={true} />
		}

		return <Animated.View style={[css.toolbarView, {transform: [{translateY: this.state._animatedMenu}]}]}>
			<Toolbar css={news.toolbar}  action={Actions.news} listViewButton={true}   newsLayoutButton={true} cardButton={true} listButton={true} />

			<View style={news.menuView}>
				<ScrollView directionalLockEnabled={true}
				            showsHorizontalScrollIndicator={false}
				            horizontal={true}>
					<TouchableOpacity
						onPress={ this.setActiveCategory.bind(this, null) }
						style={[news.menuItemView, this.state.categoryActive == null ? news.menuItemActive : null]}>
						<Text
							style={[news.menuItem, this.state.categoryActive == null ? news.menuActiveText : null]}>{"All"}</Text>
					</TouchableOpacity>

					{typeof this.state.categories != 'undefine' ?
					 this.state.categories.map((category, i) => {
						return (
							<TouchableOpacity
								onPress={this.setActiveCategory.bind(this, category.id) }
								key={category.id}
								style={[news.menuItemView, this.state.categoryActive == category.id ? news.menuItemActive : null]}>
								<Text
									style={[news.menuItem, this.state.categoryActive == category.id ? news.menuActiveText : null]}>
									{this.upCaseTitle(category.name) }
								</Text>
							</TouchableOpacity>)
					}) : <View />}

				</ScrollView>
			</View>
		</Animated.View>
	}
}
