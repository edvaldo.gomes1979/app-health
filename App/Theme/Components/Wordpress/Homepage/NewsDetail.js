'use strict';

import React, {Component} from "react";
import {Platform, Text, View, Image, Dimensions} from "react-native";
import {Actions} from "react-native-router-flux";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import HTML from "react-native-render-html";
import style from "./style";
import {Toolbar} from "@controls";
import {LinearGradient} from 'expo';

export default class NewsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected1: 'key1'
    }
  }

  onValueChange(key: string, value: string) {
    const newState = {};
    newState[key] = value;
    this.setState(newState);
  }

  getDescription(desc) {
    return desc.replace('<p>', '').substring(0, 200);
  }

  render() {
    return (
      <View style={style.color}>
        <ParallaxScrollView
          backgroundColor="white"
          contentBackgroundColor="white"
          parallaxHeaderHeight={380}
          renderFixedHeader={() => (
            <Toolbar name="" action={Actions.news} isChild="true"/>
          )}
          renderBackground={() => (
            <View style={Platform.OS !== 'android' && {paddingTop: 50}}>
              <Image style={style.productItem}
                     source={{uri: this.props.post._embedded["wp:featuredmedia"][0]['media_details']['sizes']['medium_large']['source_url']}}/>
            </View>
          )}
          renderForeground={() => (
            <View style={style.detailPanel}>
              <LinearGradient style={style.linearGradient} colors={["rgba(0,0,0,0)", "rgba(0,0,0, 0.8)"]}>
                <Text style={style.detailDesc}>{this.getDescription(this.props.post.title.rendered)}
                </Text>
              </LinearGradient>
            </View>
          )}>

          <View style={style.html}>
            <HTML html={this.props.post.content.rendered}
                  imagesMaxWidth={Dimensions.get('window').width}/>
          </View>
        </ParallaxScrollView>
      </View>
    );
  }
}
