'use strict';

import React, {Component} from "react";
import {FlatList} from "react-native";
import Post from "@wordpress/Post";
import css from "./style";

import data from '@mockdata/posts';
import categories from '@mockdata/categories';

export default class PostFeature extends Component {
  state = {categories: categories};

  renderItem = ({item, index}) => {
    return <Post post={item}
                 categories={this.state.categories}
                 layout={this.props.layout}/>
  }

  render() {
    return <FlatList data={data}
                     keyExtractor={(item, index) => item.id}
                     contentContainerStyle={this.props.vertical ? null : css.featureListView}
                     horizontal={this.props.vertical ? false : true}
                     showsHorizontalScrollIndicator={this.props.vertical ? true : false}
                     onEndReachedThreshold={100}
                     renderItem={this.renderItem}/>
  }
}
