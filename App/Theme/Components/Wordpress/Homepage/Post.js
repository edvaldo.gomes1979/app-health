'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import post from "./style";

import TimeAgo from "react-native-timeago";
import Constants from '@common/Constants';
import {Actions} from "react-native-router-flux";
import Tools from "@common/Tools";

export default class Post extends Component {
  viewPost() {
    Actions.postDetails({post: this.props.post});
  }

  viewCategoryDetail(id)  {
    Tools.viewCateDetail(id);
  }

  render() {
    const data = this.props.post;
    const categories = this.props.categories;
    const imageURL = Tools.getImage(data);
    const postTitle = typeof data.title.rendered == 'undefined' ? '' : data.title.rendered;

    if (this.props.layout == Constants.Post.layout_one) {
      return (
        <TouchableOpacity style={post.panelOne} onPress={this.viewPost.bind(this)}>
          <Image source={{uri: imageURL}} style={post.imagePanelOne}></Image>
          <Text style={post.nameOne}>{Tools.getDescription(postTitle, 40)}</Text>
          <Text style={post.timeOne}><TimeAgo time={data.date} hideAgo={true} /></Text>
        </TouchableOpacity>
      );
    }
    else if (this.props.layout == Constants.Post.layout_two) {
      return (
        <TouchableOpacity style={post.panelTwo} onPress={this.viewPost.bind(this)}>
          <Image source={{uri: imageURL}} style={post.imagePanelTwo}></Image>
          <Text style={post.nameTwo}>{Tools.getDescription(postTitle)}</Text>
          <Text style={post.timeTwo}><TimeAgo time={data.date} hideAgo={true} /></Text>
        </TouchableOpacity>
      );
    }
    else if (this.props.layout == Constants.Post.layout_list) {
      return (
        <TouchableOpacity style={post.panelList} onPress={this.viewPost.bind(this)}>
          <Image source={{uri: imageURL}} style={post.imageList}></Image>

          <View style={post.titleList}>
            <Text style={post.nameList}>{Tools.getDescription(postTitle, 300)}</Text>
            <View style={{flexDirection: 'row'}}>
              <TimeAgo style={post.timeList} time={data.date} hideAgo={true} />
              <TouchableOpacity onPress={this.viewCategoryDetail.bind(this, data.categories[0]) }>
                <Text style={post.category}>- {categories[data.categories[0]]}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>)
    }
    return (
      <TouchableOpacity style={post.panel} onPress={this.viewPost.bind(this)}>
        <Image source={{uri: imageURL}} style={post.imagePanel}></Image>
        <Text style={post.name}>{Tools.getDescription(postTitle)}</Text>
        <Text style={post.time}><TimeAgo time={data.date} hideAgo={true} /></Text>
      </TouchableOpacity>
    );
  }
}
