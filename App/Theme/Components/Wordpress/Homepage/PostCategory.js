'use strict';
import React, {Component} from "react";
import {Text, View, Image, FlatList, TouchableOpacity} from "react-native";
import {Actions} from "react-native-router-flux";
import TimeAgo from "react-native-timeago";
import PostHome from "./PostHome";
import Banner from "./Banner";
import news from "./style";
import EventEmitter from "@themeServices/AppEventEmitter";
import {CommentIcons, Spinkit, SwipCards} from "@controls";
import Tools from "@common/Tools";
import Constants from '@common/Constants';
import data from '@mockdata/posts';

export default class PostCategory extends Component {
  constructor(props) {
    super(props);
    this.data = [];

    // this.tags = [];
    this.categoryActive = null;

    this.state = {
      postLayout: Constants.Post.simple_view,
      page: 1,
      data: [],
      limit: 5,
      hasData: false,
      isLoading: true,
      finish: false,
    }
  }

  componentWillMount() {
    // this.fetchTags();
    EventEmitter.addListener('posts.cards.fetchData', this.fetchPostData.bind(this)); // Register event listener
    EventEmitter.addListener('posts.updateCategory', this.updateCategory.bind(this));
    EventEmitter.addListener('news.changeLayout', this.changeLayout.bind(this));
  }

  changeLayout(layout) {
    this.setState({postLayout: layout});
    this.fetchPostData(true);
  }

  /**
   * Update current active category from the menu
   * @param data
   */
  updateCategory(data) {
    if (typeof data != 'undefined') {
      this.categoryActive = data.category;
    }

    // reload the post
    this.fetchPostData(true)
  }

  fetchPostData(isReload) {
    const self = this;

    if (typeof isReload == 'undefined' && (this.state.finish || this.state.isLoading)) {
      return;
    }

    const postData = {
      'per_page': this.state.limit,
      'page': this.state.page,
      'sticky': false
    };

    if (this.categoryActive !== null) {
      postData['categories'] = this.categoryActive;
    }

    if (typeof isReload !== 'undefined') {
      postData.page = 1;
      this.setState({
        page: 1
      });

      self.data = [];
    }
  }

  /**
   * Render row for the list view
   * @param post
   * @param sectionID
   * @param rowID
   * @returns {*}
   */
  renderItem = ({item: post}) => {
    if (typeof (post.title) == 'undefined') {
      return null;
    }

    const imageUrl = Tools.getImage(post);

    const authorName = post._embedded.author[0]['name'];

    const commentCount = typeof post._embedded.replies == 'undefined' ? 0 : post._embedded.replies[0].length;

    const postTitle = typeof post.title == 'undefined' ? '' : post.title.rendered;

    const postContent = typeof post.excerpt == 'undefined' ? '' : post.excerpt.rendered;

    if (this.state.postLayout == Constants.Post.simple_view) {
      return (
        <TouchableOpacity style={news.panelList} onPress={Actions.postDetails.bind(this, {post: post})}>
          <Image source={{uri: imageUrl}} style={news.imageList}></Image>
          <View style={news.titleList}>
            <Text style={news.nameList}>{Tools.getDescription(postTitle, 300)}</Text>
            <Text style={news.descriptionList}>{Tools.getDescription(postContent, 120)}</Text>

            <View style={{flexDirection: 'row'}}>
              <TimeAgo style={news.timeList} time={post.date} hideAgo={true}/>
              <Text style={news.category}>by {authorName}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )
    }

    // large view layout
    if (this.state.postLayout == Constants.Post.list_view) {
      return (
        <TouchableOpacity elevation={10} style={news.cardNews}
                          onPress={Actions.postDetails.bind(this, {post: post})}>
          <View style={news.cardView}>
            <Image style={news.largeImage} source={{uri: imageUrl}}></Image>

            <Text style={news.newsTitle}>{Tools.getDescription(postTitle) }</Text>

            <View style={news.description}>
              <Text style={news.author}><TimeAgo time={post.date} hideAgo={true}/> by @{authorName}</Text>
              <CommentIcons post={post} comment={commentCount}/>
            </View>
          </View>
        </TouchableOpacity>
      )
    }

    // list view layout Constants.Post.narrow_view
    return (
      <TouchableOpacity style={news.smCardNews} onPress={Actions.postDetails.bind(this, {post: post})}>
        <View style={news.cardView}>
          <Image style={news.smImage} source={{uri: imageUrl}}></Image>
          <View style={news.smDescription}>
            <Text style={news.smTitle}>{Tools.getDescription(postTitle) }</Text>
            <Text style={news.smAuthor}><TimeAgo time={post.date} hideAgo={true}/> by @{authorName}</Text>
          </View>

          <CommentIcons hideOpenIcon={true} post={post} style={news.smShareIcons} comment={commentCount}/>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    // default homepage
    if (this.categoryActive == null && this.state.postLayout == Constants.Post.simple_view) {
      return <View style={news.body}>
        <Banner />
        <PostHome />
      </View>
    }

    // category list view page
    return (
      <View style={news.body}>
        {this.state.hasData && this.state.postLayout == Constants.Post.card_view &&
        <SwipeCards data={this.state.data}/> }

        {this.state.postLayout != Constants.Post.card_view &&

        <FlatList
          contentContainerStyle={this.state.postLayout != Constants.Post.simple_view && news.listView}
          enableEmptySections={true}
          data={data}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderItem}/> }

        <Spinkit isLoading={this.state.isLoading}/>
      </View>
    );
  }
}
