'use strict';

import React, {Component} from "react";
import {Text, View} from "react-native";
import style from "./style";
import PostFeature from "./PostFeature";
import Constants from "@common/Constants";

export default class PostHome extends Component {
  render() {
    return (
			<View style={style.hlist}>
				<PostFeature id="kone" layout={Constants.Post.layout_three}/>

				<View>
					<Text style={style.title}>{"Fearture"}</Text>
					<Text style={style.titleSmall}>{"Power by Beonews"}</Text>
				</View>
				<PostFeature id="ktwo" layout={Constants.Post.layout_two}/>

				<View>
					<Text style={style.title}>{"Editor Choice"}</Text>
					<Text style={style.titleSmall}>{"Power by Beonews"}</Text>
				</View>
				<PostFeature id="kthree" layout={Constants.Post.layout_one}/>

				<View>
					<Text style={style.title}>{"Recents Post"}</Text>
					<Text style={style.titleSmall}>{"Power by Beonews"}</Text>
				</View>

				<PostFeature id="klist"
										 vertical={true}
										 layout={Constants.Post.layout_simpleListView}/>
			</View>
    );
  }
}
