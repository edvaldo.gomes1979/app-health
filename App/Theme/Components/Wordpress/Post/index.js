'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import styles from "./styles";
import {LinearGradient} from 'expo';

import TimeAgo from "react-native-timeago";
import Constants from '@common/Constants';
import {Actions} from "react-native-router-flux";
import Tools from "@common/Tools";
import {CommentIcons} from "@controls";

export default class Post extends Component {
  viewPost = () => {
    Actions.postDetails({post: this.props.post});
  }

  viewCategoryDetail = (id) => {
    Tools.viewCateDetail(id);
  }

  render() {
    const {post, categories, layout} = this.props;

    const imageURL = Tools.getImage(post);
    const authorName = post._embedded.author[0]['name'];
    const postTitle = typeof post.title.rendered == 'undefined' ? '' : post.title.rendered;
    const commentCount = typeof post._embedded.replies == 'undefined' ? 0 : post._embedded.replies[0].length;

    if (layout === Constants.Post.layout_oneColumn) {
      return <TouchableOpacity
        elevation={10}
        style={styles.cardNews}
        onPress={this.viewPost}>
        <View style={styles.cardView}>
          <Image style={styles.largeImage} source={{uri: imageURL}}/>

          <LinearGradient style={styles.linearGradient} colors={["rgba(0,0,0,0)", "rgba(0,0,0, 0.7)"]}>
            <Text style={styles.newsTitle}>{Tools.getDescription(postTitle)}</Text>
          </LinearGradient>

          <View style={styles.description}>
            <Text style={styles.author}><TimeAgo time={post.date} hideAgo={true}/> by @{authorName}</Text>
            <CommentIcons post={post} comment={commentCount}/>
          </View>
        </View>
      </TouchableOpacity>
    }
    else if (layout === Constants.Post.layout_twoColumn) {
      return (
        <TouchableOpacity style={styles.smCardNews}
                          onPress={this.viewPost.bind(this)}>
          <View style={styles.cardView}>
            <Image style={styles.smImage} source={{uri: imageURL}}/>
            <View style={styles.smDescription}>
              <Text style={styles.smTitle}>{Tools.getDescription(postTitle) }</Text>
              <Text style={styles.smAuthor}><TimeAgo time={post.date} hideAgo={true}/> by @{authorName}</Text>
            </View>
            <CommentIcons hideOpenIcon={true} post={post} style={styles.smShareIcons} comment={commentCount}/>
          </View>
        </TouchableOpacity>);
    }
    else if (layout == Constants.Post.layout_simpleListView) {
      return (
        <TouchableOpacity style={styles.panelList} onPress={this.viewPost.bind(this)}>
          <Image source={{uri: imageURL}} style={styles.imageList}/>
          <View style={styles.titleList}>
            <Text style={styles.nameList}>{Tools.getDescription(postTitle, 300)}</Text>

            <View style={{flexDirection: 'row', marginLeft: 5, marginTop: 8}}>
              <Text style={{fontSize: 12, color: '#999'}}>
                <TimeAgo style={styles.timeList} time={post.date}
                         hideAgo={true}/> by @{authorName}
              </Text>
              <TouchableOpacity onPress={() => this.viewCategoryDetail(post.categories[0]) }>
                <Text style={styles.category}>{categories[post.categories[0]]}</Text>
              </TouchableOpacity>
            </View>
            <CommentIcons hideOpenIcon={true} post={post} comment={commentCount}/>
          </View>
        </TouchableOpacity>)
    }

    return (
      <TouchableOpacity style={styles.panel} onPress={this.viewPost.bind(this)}>
        <Image source={{uri: imageURL}} style={styles.imagePanel}/>
        <Text style={styles.name}>{Tools.getDescription(postTitle)}</Text>
        <Text style={styles.time}><TimeAgo time={post.date} hideAgo={true}/></Text>
      </TouchableOpacity>
    );
  }
}
