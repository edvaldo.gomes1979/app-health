'use strict';
import React, {Component} from "react";
import {View} from "react-native";
import {SwipeCards, Toolbar} from "@controls";
import data from '@mockdata/posts';
import css from "@wordpress/Homepage/style";

export default class CardView extends Component {
  render() {
    return (
      <View style={css.body}>
        <Toolbar newsLayoutButton={true}
                 cardButton={true}
                 listButton={true}
                 listViewButton={true}/>
        <SwipeCards data={data}/>
      </View>);
  }
}
