import _SimpleListView from "./SimpleListView";
import _CardView from "./CardView";
import _OneColumnView from "./OneColumnView";
import _TwoColumnView from "./TwoColumnView";

export const SimpleListView = _SimpleListView
export const CardView = _CardView
export const OneColumnView = _OneColumnView
export const TwoColumnView = _TwoColumnView