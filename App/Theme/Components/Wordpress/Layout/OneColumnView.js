'use strict';

import React, {Component} from "react";
import {View, ScrollView} from "react-native";
import Post from "@wordpress/Post";
import Toolbar from "@controls/Toolbar";
import data from '@mockdata/posts';
import Constants from '@common/Constants';

export default class OneColumnView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      vertical: false,
      categories: null,
      datasource: data
    };
  }

  render() {
    return (<View>
      <Toolbar newsLayoutButton={true} cardButton={true} listButton={true} listViewButton={true}/>
      <View >
        <ScrollView>
          {this.state.datasource.map((post, index) => {
            return <Post key={index.toString()}
                         post={post}
                         categories={this.state.categories}
                         layout={Constants.Post.layout_oneColumn}/>
          })}
        </ScrollView>
      </View>
    </View>);
  }
}
