'use strict';
import React, {Component} from "react";
import {FlatList, View} from "react-native";
import Post from "@wordpress/Post";
import {Toolbar} from "@controls";
import Constants from '@common/Constants';
import data from '@mockdata/posts';
import categories from '@mockdata/categories';

export default class SimpleListView extends Component {
  renderItem = ({item}) => <Post post={item}
                                 categories={categories}
                                 layout={Constants.Post.layout_simpleListView}/>

  render() {
    return (
      <View style={{flex: 1}}>
        <Toolbar newsLayoutButton cardButton listButton listViewButton/>
        <FlatList
          data={data}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderItem}
        />
      </View>);
  }
}
