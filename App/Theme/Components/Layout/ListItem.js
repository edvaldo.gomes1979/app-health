'use strict';

import React, {Component} from 'react';
import {View, Image, TouchableOpacity, ScrollView} from 'react-native';
import {Actions} from "react-native-router-flux";
import styles from './styles'

export default class ListItem extends Component {

  render() {
    return (
      <View style={styles.list}>
        <ScrollView>

          <TouchableOpacity onPress={Actions.product}>
            <View style={styles.blockFull}>
              <Image source={require('@themeImages/shop1.png')} style={styles.imageFull}/>
            </View>
          </TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.blockFull}>
              <Image source={require('@themeImages/shop2.png')} style={styles.imageFull}/>
            </View>
          </TouchableOpacity>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop3.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop4.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop5.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop6.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop7.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop8.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop9.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.block}>
                <Image source={require('@themeImages/shop3.png')} style={styles.imageBlock}/>
              </View>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </View>
    );
  }
}
