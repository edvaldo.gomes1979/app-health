'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import styles from './styles'

export default class Product extends Component {
  render() {
    return (
      <TouchableOpacity>
        <View style={styles.panel}>
          <Image source={this.props.imageURL} style={styles.imagePanel}></Image>
        </View>

        <Text style={styles.name}>{this.props.name}</Text>
        <Text style={styles.price}>{this.props.price}</Text>
      </TouchableOpacity>
    );
  }

}
