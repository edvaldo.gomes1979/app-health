'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import {Actions} from "react-native-router-flux";
import Icon from "@expo/vector-icons/EvilIcons";
import css from "@styles/style";
import AppEventEmitter from "@themeServices/AppEventEmitter";
import IconImage from "./IconImage";
import Constants from '@common/Constants';

export default class Toolbar extends Component {
    open() {
        AppEventEmitter.emit('hamburger.click');
    }

    render() {
        const self = this;

        const homeButton = function () {
            if (typeof self.props.isChild != 'undefined') {
                return (
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <TouchableOpacity onPress={self.props.action ? self.props.action : Actions.product}>
                            <Icon name={'chevron-left'} style={[css.icon, css.iconBack]}/><Text
                            style={{marginLeft: 20}}>back</Text>
                        </TouchableOpacity>
                    </View>
                );
            }
            
            if(self.props.logo){
              return (
                <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center', flex: 1}}>
                  <Image style={css.logo} source={require('@images/logo.png')}/>
                </View>
              );
            }
        };

        return (
            <View style={css.toolbarMenu}>
                {homeButton()}

                <View style={css.toolbarTitleView}>
                    <Text style={css.toolbarTitle}>{this.props.name}</Text>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  {/*wordpress menu*/}
                  {self.props.cardButton &&
                  <IconImage action={Actions.cardView.bind(this)}
                             image={{uri: Constants.icons.card}} /> }
                  {self.props.listButton &&
                  <IconImage action={Actions.twoColumnView.bind(this)}
                             image={{uri: Constants.icons.list}} /> }
                  {self.props.newsLayoutButton &&
                  <IconImage action={Actions.oneColumnView.bind(this)}
                             image={{uri: Constants.icons.layout}} />}
                  {self.props.listViewButton &&
                  <IconImage action={Actions.simpleListView.bind(this)}
                             image={{uri: Constants.icons.listView}} />}

                  {/*e-commerce menu*/}
                  {self.props.gridButton ?
                    <IconImage action={Actions.product}
                               image={require('@themeImages/icon-list.png')} /> : null }
                  {self.props.heartButton ?
                    <IconImage action={Actions.wishlist}
                               image={require('@themeImages/icon-heart.png')} /> : null }
                  {self.props.cartButton ?
                    <IconImage action={Actions.cart}
                               image={require('@themeImages/icon-bag.png')} /> : null }
                  {self.props.searchButton ? <IconImage
                    image={require('@themeImages/icon-search.png')} /> : null }
        				</View>
            </View>
        );
    }
}
