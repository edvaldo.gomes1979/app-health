import React, {Component} from "react";
import {Text, StyleSheet, Dimensions, TouchableOpacity} from "react-native";
const {width} = Dimensions.get("window");

const styles = StyleSheet.create({
  "buttonRound": {
    "backgroundColor": "#fff",
    "paddingTop": 12,
    "paddingRight": 12,
    "paddingBottom": 12,
    "paddingLeft": 12,
    "borderColor": "#ddd",
    "borderWidth": 1,
    "alignSelf": "center",
    "borderRadius": 20,
    "marginTop": 12,
    "marginLeft": 10,
    "marginRight": 10,
    "width": width - 30
  },
  "buttonRoundText": {
    "color": "#333",
    "alignSelf": "center",
    "fontSize": 14
  },
});

export default class ButtonRound extends Component {
  render = () => {
    let customStyle     = {},
        textCustomStyle = {};
    if(this.props.disabled !== undefined){
      customStyle = this.props.disabled ? {"opacity": 0.65} : {};
    }

    if(this.props.bg){
      customStyle['backgroundColor'] = this.props.bg;
    }

    if(this.props.color){
      textCustomStyle['color'] = this.props.color;
    }

    return (
      <TouchableOpacity
        style={[styles.buttonRound, this.props.style, customStyle]}
        onPress={this.props.onPress}
        disabled={this.props.disabled === undefined ? false : this.props.disabled}
      >
        <Text style={[styles.buttonRoundText, textCustomStyle]}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}
