import React, {Component} from "react";
import {View, Text} from "react-native";
import css from "./style";

export default class Index extends Component {
  render() {
    return (
      <View style={[css.spinner, typeof this.props.css != 'undefined' ? this.props.css : null]}>
        <Text>Loading...</Text>
      </View>
    )
  }
}
