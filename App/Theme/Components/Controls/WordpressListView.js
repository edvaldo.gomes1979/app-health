'use strict';

import React, {Component} from "react";
import {Text, FlatList, View, Image} from "react-native";
import ApiNews from "@themeServices/ApiNews";
import news from "@styles/news";

export default class WordpressListView extends Component {
  constructor(props) {
    super(props);
    this.data = [];
    this.state = {
      page: 1,
      limit: 5,
      isOnline: true,
      isLoading: false,
      finish: false,
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    var self = this;
    if (this.state.finish || !this.state.isOnline) {
      return;
    }
    self.setState({isLoading: true});

    ApiNews.getPosts({
      'per_page': this.state.limit,
      'page': this.state.page
    })
      .then(function (data) {
        console.log(data);

        self.data = self.data.concat(data);
        self.setState({
          page: self.state.page + 1,
          finish: data.length < self.state.limit,
          isLoading: false,
        });
      });
  }


  onEndReached() {
    this.fetchData();
  }

  renderItem = ({item: post, index: rowID}) => {
    var oddStyle = rowID % 2 == 0 ? {backgroundColor: '#F8F8F8'} : '';

    if (post.better_featured_image != null) {
      return (
        <View style={[news.card, oddStyle]}>
          <Image style={news.image} source={{uri: post.better_featured_image.source_url}}></Image>

          <View style={news.content}>
            <Text>{post.title.rendered}</Text>
            <Text>{post.date}</Text>
          </View>
        </View>
      );
    }
    return (
      <View style={[news.card, oddStyle]}>
        <View style={news.box}></View>
        <View style={news.content}>
          <Text>{post.title.rendered}</Text>
          <Text>{post.date}</Text>
        </View>
      </View>
    );
  }

  render() {
    return (
      <FlatList
        data={this.data}
        keyExtractor={(item, index) => item.id}
        style={news.body}
        onEndReached={this.onEndReached.bind(this)}
        dataSource={this.state.dataSource}
        renderItem={this.renderItem}/>
    );
  }
}
