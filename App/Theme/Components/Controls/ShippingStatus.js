'use strict';
import React, {Component} from "react";
import {View, StyleSheet, Dimensions} from "react-native";
const {width} = Dimensions.get("window");

const shipping = StyleSheet.create({
  "statusIcon": {
    "height": 20,
    "width": 20,
    "paddingTop": 6,
    "paddingRight": 6,
    "paddingBottom": 6,
    "paddingLeft": 6,
    "backgroundColor": "#eee",
    "borderRadius": 20,
    "marginTop": -14,
    "borderWidth": 6,
    "borderColor": "transparent"
  },
  "statusActive": {
    "backgroundColor": "#1CAADE",
    "borderWidth": 6,
    "borderColor": "#eee",
    "borderRadius": 30
  },
  "statusFinish": {
    "backgroundColor": "#1CAADE",
    "borderWidth": 6,
    "borderColor": "#1CAADE",
    "borderRadius": 20
  },
  "status": {
    "borderTopWidth": 2,
    "borderColor": "#eee",
    "width": width - 80,
    "position": "relative",
    "alignSelf": "center",
    "marginTop": 20,
    "marginBottom": 30,
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
});

export default class ShippingStatus extends Component {
  render() {
    return (
      <View>
        <View style={shipping.status}>
          <View style={[shipping.statusIcon, shipping.statusActive]}/>
          <View style={[shipping.statusIcon, shipping.statusFinish]}/>
          <View style={[shipping.statusIcon]}/>
        </View>
        <View style={[shipping.status, shipping.statusFull]}/>
      </View>
    );
  }
}