'use strict';
import React, {Component} from "react";
import {View, Text} from 'react-native';
import css from './style';
import {SimpleLineIcons} from '@expo/vector-icons';

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ColorHeart: typeof this.props.color == 'undefined' ? '#333' : this.props.color
    };
  }

  render() {
    const textColor = typeof this.props.color == 'undefined' ? '#333' : this.props.color;
    return (
      <View
        style={typeof (this.props.style) == 'undefined' || this.props.style == null ? css.shareIcon : this.props.style}>

        {typeof this.props.hideLoveIcon == 'undefined' ?
          <SimpleLineIcons.Button style={css.newsIcons}
                                  name="heart"
                                  size={16}
                                  color={this.state.ColorHeart}
                                  backgroundColor="transparent">
          </SimpleLineIcons.Button>
          : null }

        {typeof this.props.hideShareIcon == 'undefined' ?
          <SimpleLineIcons.Button style={css.newsIcons}
                                  name="share"
                                  size={16}
                                  color={textColor}
                                  backgroundColor="transparent">
          </SimpleLineIcons.Button>
          : null }

        {typeof this.props.hideOpenIcon == 'undefined' ?
          <SimpleLineIcons.Button style={css.newsIcons}
                                  name="share-alt"
                                  size={16}
                                  color={textColor}
                                  backgroundColor="transparent">
          </SimpleLineIcons.Button>
          : null }

        {typeof this.props.hideCommentIcon == 'undefined' ?
          <SimpleLineIcons.Button style={css.newsIcons}
                                  name="speech"
                                  size={16}
                                  color={textColor}
                                  backgroundColor="transparent">
            <Text style={[css.iconText, {color: textColor}]}>{this.props.comment}</Text>
          </SimpleLineIcons.Button>
          : null }

      </View>
    )
  }
}
