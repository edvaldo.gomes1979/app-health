'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import {Actions} from "react-native-router-flux";
import Icon from "@expo/vector-icons/EvilIcons";
import css from "@styles/style";
import AppEventEmitter from "@themeServices/AppEventEmitter";
import IconImage from "./IconImage";
import Constants from '@common/Constants';

export default class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {layout: 3}
  }

  open = () => {
    AppEventEmitter.emit('hamburger.click');
  }

  changeLayout = () =>  {
    AppEventEmitter.emit('news.changeLayout');
  }

  render() {
    const {
      isChild, action,
      cardButton, listButton, newsLayoutButton, listViewButton,
      gridButton, heartButton, cartButton, searchButton
    } = this.props;

    const homeButton = () => {
      if (typeof isChild != 'undefined') {
        return (
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity hitSlop={hitSlop} onPress={action ? action : Actions.news}>
              <Icon name={'chevron-left'} style={[css.icon, css.iconBack]}/><Text
              style={{marginLeft: 20}}>back</Text>
            </TouchableOpacity>
          </View>
        );
      }

      if(this.props.logo){
        return (
          <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <Image style={css.logo} source={require('@images/logo.png')}/>
          </View>
        );
      }
    };
    const hitSlop = {top: 20, right: 20, bottom: 20, left: 20};
    return (
      <View style={css.toolbarMenu}>
        {homeButton()}

        <View style={css.toolbarTitleView}>
          <Text style={css.toolbarTitle}>{this.props.name}</Text>
        </View>

        <View style={{flexDirection: 'row', alignItems: 'center'}}>

          {/*wordpress menu*/}
          {cardButton && <IconImage action={Actions.cardView.bind(this)}
                                    image={{uri: Constants.icons.card}}/> }
          {listButton && <IconImage action={Actions.twoColumnView.bind(this)}
                                    image={{uri: Constants.icons.list}}/> }
          {newsLayoutButton && <IconImage action={Actions.oneColumnView.bind(this)}
                                          image={{uri: Constants.icons.layout}}/>}
          {listViewButton && <IconImage action={Actions.simpleListView.bind(this)}
                                        image={{uri: Constants.icons.listView}}/>}

          {/*e-commerce menu*/}
          {gridButton ? <IconImage action={Actions.product}
                                   image={require('@themeImages/icon-list.png')}/> : null }
          {heartButton ? <IconImage action={Actions.wishlist}
                                    image={require('@themeImages/icon-heart.png')}/> : null }
          {cartButton ? <IconImage action={Actions.cart}
                                   image={require('@themeImages/icon-bag.png')}/> : null }
          {searchButton ? <IconImage image={require('@themeImages/icon-search.png')}/> : null }
        </View>
      </View>
    );
  }
}
