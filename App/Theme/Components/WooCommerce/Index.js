'use strict';

import React, {Component} from "react";
import {
  TextInput,
  View,
  Animated,
  ScrollView,
  FlatList
} from "react-native";
import {Toolbar} from "@controls";
import Api from "@themeServices/Api";
import styles from "./styles";
import ProductItem from "./ProductItem";

var offset = 0;
var offsetHeader = 100;
var beta = 50;

export default class WooProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 10,
      isOnline: true,
      isLoading: false,
      finish: false,
      _animatedMenu: new Animated.Value(0),
      data: []
    }
  }

  hideMenu() {
    Animated.spring(this.state._animatedMenu, {
      toValue: -120
    }).start();
  }

  showMenu() {
    Animated.spring(this.state._animatedMenu, {
      toValue: 0
    }).start();
  }

  onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;

    if (currentOffset < offsetHeader) {
      return;
    }

    if (Math.abs(offset - currentOffset) <= beta)
      return;


    if (currentOffset > offset) {
      this.hideMenu();
    } else if (currentOffset < offset) {
      this.showMenu()
    }
    offset = currentOffset;
  }

  componentDidMount() {
    // check internet connecting
    // NetInfo.isConnected.fetch().done(isConnected => {
    //     this.setState({isOnline: isConnected});
    // });
    this.fetchData();
  }

  fetchData = async () => {
    const {limit, page, finish, isOnline, data} = this.state;

    if (finish || !isOnline) {
      return;
    }
    this.setState({isLoading: true});
    let self = this;

    const response = await Api.get('products', {per_page: limit, page: page});
    const responseJson = await response.json();

    self.setState({
      page: page + 1,
      finish: data.length < limit,
      isLoading: false,
      data: data.concat(responseJson)
    });
  }


  getDataSource(products) {
    return this.state.dataSource.cloneWithRows(products);
  }

  onEndReached = () => {
    console.log('get more data')
    this.fetchData();
  }

  renderItem = ({item, index}) => {
    return <ProductItem key={'row' + item.id} product={item}/>
  }

  render() {
    return (
      <View style={styles.productColor}>
        <Animated.View style={[styles.toolbarView, {transform: [{translateY: this.state._animatedMenu}]}]}>
          <Toolbar name="Product" heartButton={true}/>
          <TextInput style={styles.inputSearch} underlineColorAndroid='rgba(0,0,0,0)' placeholder={'Search'}/>
        </Animated.View>

        <ScrollView
          style={{paddingTop: 106}}
          onScroll={this.onScroll.bind(this)} scrollEventThrottle={30}>
          <FlatList
            contentContainerStyle={{flex: 1}}
            data={this.state.data}
            keyExtractor={(item, index) => item.id}
            renderItem={this.renderItem}

            onEndReachedThreshold={100}
            onEndReached={(distance) => distance.distanceFromEnd > 100 && this.onEndReached()}
          />
        </ScrollView>
      </View>
    );
  }
}
