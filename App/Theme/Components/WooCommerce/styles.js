import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import Color from '@common/Color';
import Constants from '@common/Constants';
const {width, height, scale} = Dimensions.get("window"),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh);

export default StyleSheet.create({
  "star": {
    "position": "absolute",
    "right": 15,
    "top": 15
  },

  "productItem": {
    "width": width-30,
    "height":  (width-20)*962/875,
    "marginTop": 5,
    "marginRight": 5,
    "marginBottom": 5,
    "marginLeft": 5
  },

  "discountPrice": {
    "color": "#535353",
    "paddingLeft": 5,
    "fontSize": 12,
    "fontWeight": "600",
    "marginLeft": 1,
    "marginRight": 1,
    "paddingBottom": 8
  },
  "cards": {
    "position": "relative",
    "backgroundColor": "#fff",
    "width": width -20,
    "alignItems": "center",
    "justifyContent": "center",
    "marginLeft": 10,
    "marginRight": 10,
    "marginTop": 5,
    "marginBottom": 5,
    "shadowColor": "#000",
    "borderRadius": 2
  },
});