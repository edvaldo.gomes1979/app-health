import WooCommerceAPI from "./WooCommerceAPI"
import Constants from './../Common/Constants';

var Api = new WooCommerceAPI({
  url: Constants.URL.root,
  consumerKey: Constants.Keys.ConsumeKey,
  consumerSecret: Constants.Keys.ConsumerSecret,
  wp_api: true,
  version: 'wc/v2',
});

export default Api;
