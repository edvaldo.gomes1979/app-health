import React, {Component} from "react";
import {
  View,
  ActivityIndicator,
} from "react-native";
import css from "@styles/style";
import {Toolbar} from "@controls";

export default class Profile extends Component {

  render() {
    return (
      <View style={css.layout}>
        <Toolbar name="Perfil"/>
        <ActivityIndicator size="large" />
      </View>
    );
  }
}
