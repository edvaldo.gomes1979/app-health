import React, {Component} from "react";
import {
  View,
  Image,
  Dimensions,
  ActivityIndicator,
  StatusBar
} from "react-native";
import {Actions} from "react-native-router-flux";
import {
  User
} from '@services/User'

const { width } = Dimensions.get("window"),
      css = {
        layout: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#FFFFFF'
        },
        logo: {
          width: width - 100,
          resizeMode: 'contain',
        }
      };

export default class Splash extends Component {
  
  componentDidMount() {
    User.load((data) => {
      if(data.id){
        return Actions.dashboard();
      }

      return Actions.auth();
    });
  }

  render() {
    return (
      <View style={css.layout}>
        <StatusBar hidden />
        <Image style={css.logo} source={require('@images/logo.png')}/>
        <ActivityIndicator size="large" />
      </View>
    );
  }
}
