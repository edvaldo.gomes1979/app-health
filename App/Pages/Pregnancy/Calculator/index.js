import React, {Component} from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Text,
  ActivityIndicator,
  Alert
} from "react-native";

import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { Actions } from "react-native-router-flux";

import styles from "./styles";
import css from "@styles/style";

import {Toolbar, ButtonRound, ButtonRoundBlue} from "@controls";
import Messages from "@constants/Messages";

import PregnancyCalculatorService from '@services/Pregnancy/Calculator';


export default class PregnancyCalculator extends Component {
  state = {
    page: 'calculator',
    isDateTimePickerVisible: false,
    calculator: {
      last_period: null
    },
    minDate: moment().subtract(9, 'months').toDate(),
    maxDate: moment().toDate(),
    loading: false,
    calculatorResult: {}
  };

  showDateTimePicker(){
    this.setState({ isDateTimePickerVisible: true });
  }

  hideDateTimePicker(){
    this.setState({ isDateTimePickerVisible: false });
  }

  handleDatePicked(date){
    this.state.calculator.last_period = moment(date).format('DD/MM/YYYY');
    this.setState(this.state);

    this.hideDateTimePicker();
  };

  handleCalculate(){
    this.handleLoadingStart();    

    PregnancyCalculatorService.create({calculator: this.state.calculator}, (result) => {
      this.handleLoadingFinish();

      if(result.error){
        return Alert.alert(`${Messages.generic.error} [${result.status}]`);
      }

      result.calculated.expected.date = moment(result.calculated.expected.date, 'YYYY-MM-DD').format('DD/MM/YYYY');
      this.state.calculatorResult = result;
      this.state.page = 'calculator_result';
      this.setState(this.state);

    })
    .catch((result) => {
      Alert.alert(Messages.generic.error);
      this.handleLoadingFinish();
    });
  }

  handleLoadingStart(){
    this.state.loading = true;
    this.setState(this.state);
  }

  handleLoadingFinish(){
    this.state.loading = false;
    this.setState(this.state);
  }

  render() {
    return (
      <View style={styles.layout}>
        <Toolbar logo={true} />
        <ScrollView>
          {this.state.page === 'calculator' ? (
            <View>
              <View style={styles.moduleContainer}>
                <View style={styles.iconContainer}>
                  <Image source={require('@images/pregnant/couple.png')} style={styles.icon}/>
                </View>
                <View style={styles.iconTextContainer}>
                  <Text style={styles.h1}>Calculadora de Gravidez</Text>
                </View> 
              </View>

              <View style={styles.card}>
                <TouchableOpacity onPress={this.showDateTimePicker.bind(this)}>
                  <Text style={styles.fakeInput}>{!this.state.calculator.last_period ? '1º dia do seu último período' : this.state.calculator.last_period}</Text>
                </TouchableOpacity>

                <DateTimePicker
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this.handleDatePicked.bind(this)}
                  onCancel={this.hideDateTimePicker.bind(this)}
                  minimumDate={this.state.minDate}
                  maximumDate={this.state.maxDate}
                />

                <View>
                  {!this.state.loading ? (
                    <View style={{"flexDirection": "row", "alignSelf": "center"}}>
                      <ButtonRound style={css.halfWidth} text="Voltar"  onPress={() => Actions.dashboard()} color="#FFF" bg="#EC466A"/>
                      <ButtonRound style={css.halfWidth} disabled={!this.state.calculator.last_period} text="Calcular" onPress={this.handleCalculate.bind(this)}/>
                    </View>
                  ) : (
                    <ActivityIndicator size="large" color="#FFFFFF"/>
                  )}
                </View>
              </View>
            </View>
          ) : (
            <View>
              <View style={[styles.moduleContainer, {backgroundColor: null, height: 150}]}>
                <View style={[styles.iconContainer, {borderColor: '#FFFFFF'}]}>
                  <Image source={require('@images/pregnant/couple.png')} style={[styles.icon, {tintColor: '#FFFFFF'}]}/>
                </View>
              </View>

              <View style={styles.resultContainer}>
                <View>
                  <Text style={styles.resultTitle}>{this.state.calculatorResult.calculated.weeks.weeks}  Semanas</Text>

                  <Text style={styles.resultItem}>Data prevista para o nascimento
                    <Text style={styles.resultValue}> {this.state.calculatorResult.calculated.expected.date}</Text>
                  </Text>

                  <Text style={styles.resultItem}>Você está com
                    <Text style={styles.resultValue}> {this.state.calculatorResult.calculated.weeks.weeks} semanas </Text>
                      {this.state.calculatorResult.calculated.weeks.days > 0 && 
                        <Text style={styles.resultValue}> e 
                        <Text style={styles.resultValue}> {this.state.calculatorResult.calculated.weeks.days} dias </Text></Text>
                      }
                      de gravidez.
                  </Text>
                
                  <Text style={styles.resultItem}>Há
                    <Text style={styles.resultValue}> {this.state.calculatorResult.calculated.expected.days} </Text> 
                    dias até a data prevista para o parto.
                  </Text>
                </View>
                <ButtonRound text="Voltar" onPress={() => Actions.pregnancyCalculator()} color="#FFF" bg="#EC466A"/>
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}