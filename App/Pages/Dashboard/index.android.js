import React, {Component} from "react";
import { TabBarIOS, StatusBar }      from "react-native";

import TabIcons from '@constants/TabIcons';
import {
  Notification,
  Activities,
  Profile,
  Home
} from "@pages"

import ScrollableTabView from "react-native-scrollable-tab-view";
import FacebookTabBar from "@custom/FacebookTabBar";

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {selectedTab: 'tabOne'}
  }

  render() {
    return (
      <ScrollableTabView
        tabBarPosition={"bottom"}
        locked={true}
        renderTabBar={() => <FacebookTabBar />}
      >
        <Home tabLabel="md-home"/>
        <Activities tabLabel="md-list"/>
        <Notification tabLabel="md-notifications"/>
        <Profile tabLabel="md-person"/>
      </ScrollableTabView>
    );
  }
}