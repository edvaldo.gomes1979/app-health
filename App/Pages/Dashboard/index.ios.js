import React, {Component} from "react";
import { TabBarIOS, StatusBar }      from "react-native";

import TabIcons from '@constants/TabIcons';
import {
  Notification,
  Activities,
  Profile,
  Home
} from "@pages"



export default class  Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {selectedTab: 'tabOne'}
  }

  render() {
    return (
      <TabBarIOS
        tintColor="#1CAADE"
        translucent={true}>
        <StatusBar hidden={false} />
        <TabBarIOS.Item
          title=""
          icon={{uri: TabIcons.home, scale: 4}}
          selected={this.state.selectedTab === 'tabOne'}
          onPress={() => {
            this.setState({
              selectedTab: 'tabOne'
            });
          }}>

          <Home />
        </TabBarIOS.Item>

        <TabBarIOS.Item
          title=""
          icon={{uri: TabIcons.timeline, scale: 4}}
          selected={this.state.selectedTab === 'tabThree'}
          onPress={() => {
            this.setState({
              selectedTab: 'tabThree',
              notifCount: this.state.notifCount + 1
            });
          }}>
          <Activities />
        </TabBarIOS.Item>

        <TabBarIOS.Item
          title=""
          icon={{uri:  TabIcons.notification, scale: 4}}
          selected={this.state.selectedTab === 'tabFour'}
          onPress={() => {
            this.setState({
              selectedTab: 'tabFour',
              notifCount: this.state.notifCount + 1
            });
          }}>
          <Notification />
        </TabBarIOS.Item>

        <TabBarIOS.Item
          title=""
          icon={{uri:  TabIcons.profile, scale: 4}}
          selected={this.state.selectedTab === 'tabFive'}
          onPress={() => {
            this.setState({
              selectedTab: 'tabFive',
              notifCount: this.state.notifCount + 1
            });
          }}>
          <Profile />
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
}