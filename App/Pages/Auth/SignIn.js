'use strict';
import React, {Component} from "react";
import {
  Text,
  View,
  Dimensions,
  ActivityIndicator,
  Alert
} from "react-native";

import {Actions} from "react-native-router-flux";
import {ButtonRoundBlue, IconInput}  from "@controls";
import AuthService from '@services/Auth/Auth';
import { User } from '@services/User';
import styles from "./styles";

export default class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email:    null,
      password: null,
      loading:  false,
    };
  }

  storage(result){
    User.set(result);
    Actions.dashboard();
  }

  signIn(){
    this.state.loading = true;
    this.setState(this.state);

    AuthService.login(this.state, (result) => {
      this.state.loading = false;
      this.setState(this.state);

      if(result.status === false){
        return Alert.alert(result.message);
      }

      this.storage(result);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{marginTop: -15}}>
          <IconInput
            ref="email"
            placeholder="E-mail"
            image={require("@themeImages/icon-user.png")}
            onChangeText={(email) => this.setState({email})}
            returnKeyType="next"
            value={this.state.email}
          />

          <IconInput
            ref="password"
            placeholder="Senha"
            image={require("@themeImages/icon-password.png")}
            onChangeText={(password) => this.setState({password})}
            returnKeyType="next"
            secureTextEntry={true}
            value={this.state.password}
          />
        </View>

      
        {this.state.loading ? (
          <View style={{marginTop: 10}} >
            <ActivityIndicator size="large" />
          </View>
          ) : (
            <ButtonRoundBlue text="Entrar" onPress={this.signIn.bind(this)}/>
          )
        }

      </View>
    );
  }
}
