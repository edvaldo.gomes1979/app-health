'use strict';
import React, {Component} from "react";
import {View, Image, TouchableOpacity, Dimensions,} from "react-native";
import {Actions} from "react-native-router-flux";
import ScrollableTabView, {ScrollableTabBar} from "react-native-scrollable-tab-view";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import css from "@styles/style";

const layout = {
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
};

export default class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      visibleHeight: Dimensions.get('window').height,
      scroll: false
    };
  }

  render() {
    const toolbar = (
      <View style={[css.toolbarMenu]}>
        <Image style={css.logo} source={require('@images/logo.png')}/>
      </View>
    )
    return (
      <View style={layout}>
        {toolbar}
        <ScrollableTabView
          initialPage={0}
          locked={false}
          tabBarUnderlineStyle={ {height: 2, backgroundColor: "#1CAADE"}  }

          tabBarActiveTextColor={"#393838"}
          tabBarInactiveTextColor={"#B8B8B8"}
          tabBarTextStyle={{fontWeight: 'normal', fontSize: 14}}
          style={{backgroundColor: '#ffffff'}}
          contentProps={{}}
          renderTabBar={() => <ScrollableTabBar
            underlineHeight={3}
            style={{borderBottomColor: '#EEEEEE'}}
            tabsContainerStyle={{paddingLeft: 30, paddingRight: 30}}
            tabStyle={{paddingBottom: 0, borderBottomWidth: 0, paddingTop: 0, paddingLeft: 50, paddingRight: 50}}
          />}
        >
          <SignIn tabLabel="Acessar"/>
          <SignUp tabLabel="Cadastrar"/>
        </ScrollableTabView>
      </View>
    );
  }
}
