'use strict';

import React, {Component} from "react";
import {
  Text, 
  View, 
  ScrollView, 
  TouchableOpacity,  
  Dimensions,
  ActivityIndicator,
  Alert
} from "react-native";
import styles from "./styles";
import {ButtonRoundBlue, IconInput} from "@controls";
import {Actions} from "react-native-router-flux";
import Register from '@services/Register/Register';
import { User } from '@services/User';


export default class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email:    null,
      password: null, 
      name:null,
      phone: null,
      role_ids: '5be6fc707afccc82a568ecf7',
      company_id:'5bd48c63a7c8bd0007b95203',
      loading:  false,
    };
  }
  storage(result){
    User.set(result);
    Actions.dashboard();
  }

  valideted(){
    let valid = true;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

    if(this.state.email == null || this.state.name == null || this.state.password == null){
      alert("Olá, O Nome, E-mail e Senha são Obrigatórios para efetuar o cadastro");
      return false;
    }

    if(this.state.password.length < 6){
      alert("Olá, A senha deve conter no minimo 6 digitos");
      return false;
    }

    if(reg.test(this.state.email) === false) {
      alert("Olá, Informe um E-mail valido para efetuar o cadastro");
      return false;
    }
    return valid
  }


  signUp(){

    if(!this. valideted()){
      return false;
    }

    this.state.loading = true;
    this.setState(this.state);

    Register.sing_up({sign_up:this.state}, (result) => {
      this.state.loading = false;
      this.setState(this.state);
      if(result.erros){
        if(result.erros.email){
         let message = result.erros.email[0] == 'is already taken' ? "E-mail já esta cadastrado":'';
         return Alert.alert(message);
        }
      }
      if(result.status === false){
        return Alert.alert(result.message);
      }

      this.storage(result);
    }).catch((errors) => { 
      Alert.alert(errors.message)
    });
  }

  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
      <View style={{marginTop: -20}}>
      <IconInput
      ref="name"
      placeholder="Nome"
      image={require("@themeImages/icon-user.png")}
      onChangeText={(name) => this.setState({name})}
      returnKeyType="next"
      value={this.state.name}
      />
      <IconInput
      ref="email"
      placeholder="E-mail"
      image={require("@themeImages/icon-email.png")}
      onChangeText={(email) => this.setState({email})}
      returnKeyType="next"
      value={this.state.email}
      />
      <IconInput
      ref="phone"
      placeholder="Celular"
      image={require("@themeImages/icon-phone.png")}
      onChangeText={(phone) => this.setState({phone})}
      returnKeyType="next"
      value={this.state.phone}
      />        
      <IconInput
      ref="password"
      placeholder="Senha"
      image={require("@themeImages/icon-password.png")}
      onChangeText={(password) => this.setState({password})}
      returnKeyType="next"
      secureTextEntry={true}
      value={this.state.password}
      />
      </View>

      {this.state.loading ? (
        <View style={{marginTop: 10}} >
        <ActivityIndicator size="large" />
        </View>
        ) : (
        <ButtonRoundBlue text="Entrar" onPress={this.signUp.bind(this)}/>
        )
      }
      </View>
      </ScrollView>
      );
    }
  }
