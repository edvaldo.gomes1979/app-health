'use strict';
import React, {Component} from "react";
import {View} from "react-native";
import ScrollableTabView, {ScrollableTabBar} from "react-native-scrollable-tab-view";
import styles from "./styles";
import {Tracking, Summary} from "@templates";
import {Toolbar} from "@controls";

export default class Activities extends Component {

  render() {
    return (
      <View style={styles.body}>
        <Toolbar name="Atividades"/>

        <Tracking tabLabel="STATUS"/>
      </View>
    );
  }
}
