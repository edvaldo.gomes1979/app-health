import React, {Component} from "react";

import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import css from "@styles/style";
import {Actions} from "react-native-router-flux";

import Toolbar from "@controls/Toolbar";

export default class Home extends Component {
  render() {
    return (
      <View style={css.layout}>
        <Toolbar name="" logo={true}/> 
        <ScrollView>
          <View style={css.templateLayout}>
            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.pregnancyCalculator}>

              <Image style={css.templateImage}
                     source={require('@images/modules/pregnant.png')}></Image>
              <Text style={css.templateMenu}>Gravidez</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.schedule}
            >
              <Image style={css.templateImage} source={require('@images/modules/schedule.png')}></Image>
              <Text style={css.templateMenu}>Agenda</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.historic}
            >
              <Image
                style={css.templateImage}
                source={require('@images/modules/historic.png')}
              >
              </Image>
              <Text style={css.templateMenu}>Histórico</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.historic}>
              <Image
                style={css.templateImage}
                source={require('@images/modules/prevention.png')}>
              </Image>
              <Text style={css.templateMenu}>Prevenção</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.selfEvaluation}>
              <Image style={css.templateImage}
                     source={require('@images/modules/self-evaluation.png')}></Image>
              <Text style={css.templateMenu}>Auto Avaliação</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.map}>

              <Image style={css.templateImage}
                     source={require('@images/modules/health-promotion.png')}></Image>
              {/*<Text style={css.templateMenu}>Saúde</Text>*/}
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.activity}>

              <Image style={css.templateImage}
                     source={require('@images/modules/activity.png')}></Image>
              <Text style={css.templateMenu}>Atividade Física</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.nutrition}>

              <Image style={css.templateImage}
                     source={require('@images/modules/nutrition.png')}></Image>
              <Text style={css.templateMenu}>Nutrição</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={css.templateRow}
              onPress={Actions.family}>
              <Image style={css.templateImage}
                     source={require('@images/modules/family.png')}></Image>
              <Text style={css.templateMenu}>Quero Cuidar</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}