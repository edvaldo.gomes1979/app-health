import React from 'react';
import Routes from './App/routes';

export default class App extends React.Component {
  render() {
    return (
      <Routes />
    );
  }
}